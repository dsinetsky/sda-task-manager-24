package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_LIST_TASKS_OF_PROJECT;

    @NotNull
    public static final String DESCRIPTION = "Shows all tasks bound to project(if any)";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter Id of project:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> listTasks = getTaskService().returnTasksOfProject(userId, projectId);
        showListTask(listTasks);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
