package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class HelpDisplayCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = ArgumentConst.CMD_HELP;

    @NotNull
    public static final String NAME = TerminalConst.CMD_HELP;

    @NotNull
    public static final String DESCRIPTION = "Shows all commands";

    @Override
    public void execute() {
        listCommands(getCommandService().getTerminalCommands());
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
