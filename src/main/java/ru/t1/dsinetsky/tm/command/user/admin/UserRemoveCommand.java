package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractAdminCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_DELETE_USER;

    @NotNull
    public static final String DESCRIPTION = "Deletes user from system";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        @Nullable final String login = TerminalUtil.nextLine();
        getUserService().removeUserByLogin(login);
        System.out.println("User successfully removed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
