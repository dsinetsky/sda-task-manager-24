package ru.t1.dsinetsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedRepository<Task> {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String desc);

    @NotNull
    List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
