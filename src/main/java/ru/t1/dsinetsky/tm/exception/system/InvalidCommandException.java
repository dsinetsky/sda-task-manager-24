package ru.t1.dsinetsky.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class InvalidCommandException extends GeneralSystemException {

    public InvalidCommandException() {
        super("Invalid command! Type \"help\" for list of commands!");
    }

    public InvalidCommandException(@NotNull final String message) {
        super("Command " + message + " is not supported! Type \"help\" for list of commands!");
    }

}
