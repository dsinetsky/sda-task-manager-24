package ru.t1.dsinetsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return returnAll(userId)
                .stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public Task create(@NotNull final String userId, @NotNull final String name) {
        return add(userId, new Task(name));
    }

    @Override
    @NotNull
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        return add(userId, new Task(name, description));
    }

}
